'use strict'

const Alexa = require('alexa-sdk')
const Request = require('request')

const handlers = {
    // Use LaunchRequest, instead of NewSession if you want to use the one-shot model
    // Alexa, ask [my-skill-invocation-name] to (do something)...
  'LaunchRequest': function () {
    this.emit(':ask', 'Hey Mirum developer what is your name?', 'Please say that again?')
  },
  'myNameIsIntent': function () {
    const names = this.event.request.intent.slots.names.value
    this.emit(':tell', 'now I know your name is  ' + names)
  },
  'AMAZON.StopIntent': function () {
    this.emit('SessionEndedRequest')
  },
  'AMAZON.CancelIntent': function () {
    this.emit('SessionEndedRequest')
  },
  'SessionEndedRequest': function () {
    console.log(`Session ended: ${this.event.request.reason}`)
  },
  'Unhandled': function () {
    this.response.speak('unhandled')
    this.emit(':responseReady')
  }
}

exports.handler = function (event, context, callback) {
  const alexa = Alexa.handler(event, context, callback)
  alexa.registerHandlers(handlers)
  alexa.execute()
}
